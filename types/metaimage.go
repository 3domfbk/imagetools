package types

import "image"

// MetaValue is any value
type MetaValue struct {
	Value   interface{}
	TagType uint16
	Count   uint32
}

// Rational type
type Rational struct {
	N, D uint32
}

// SRational type
type SRational struct {
	N, D int32
}

// MetaImage is an image with arbitrary metadata
type MetaImage struct {
	Image    image.Image
	MetaData map[uint16]MetaValue
}

// NewMetaImage makes a new empty meta image
func NewMetaImage() *MetaImage {
	return &MetaImage{nil, make(map[uint16]MetaValue)}
}

// Ratio returns the rational as a float64 value
func (r Rational) Ratio() float64 {
	return float64(r.N) / float64(r.D)
}
