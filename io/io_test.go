package io

import (
	"testing"

	"bitbucket.org/3domfbk/imagetools/types"

	. "github.com/smartystreets/goconvey/convey"
)

func TestReadTiff(t *testing.T) {
	Convey("Given a Tiff file", t, func() {
		img, err := ReadMetaImage("testdata/gimp.tif")

		Convey("It should correctly load it", func() {
			So(err, ShouldBeNil)
			So(img.MetaData[256].Value, ShouldEqual, 128)
			So(img.MetaData[257].Value, ShouldEqual, 128)
			So(img.MetaData[258].Value, ShouldResemble, []uint16{8, 8, 8})
			So(img.MetaData[270].Value, ShouldResemble, "Created with GIMP")
			So(img.MetaData[279].Value, ShouldResemble, []uint32{24576, 24576})
			So(img.MetaData[282].Value.(types.Rational).Ratio(), ShouldEqual, 72)
			So(img.MetaData[283].Value.(types.Rational).Ratio(), ShouldEqual, 72)
		})

		Convey("It should correctly write it to another tiff files", func() {
			err = WriteMetaImage("testdata/gimpout.tif", img)
		})
	})
}
