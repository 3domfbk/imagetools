package io

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"io/ioutil"
	"os"
	"sort"

	"golang.org/x/image/tiff"

	"bitbucket.org/3domfbk/imagetools/types"
)

const (
	_ = iota
	tagTypeByte
	tagTypeASCII
	tagTypeShort
	tagTypeLong
	tagTypeRational
	tagTypeSByte
	tagTypeUndefined
	tagTypeSShort
	tagTypeSLong
	tagTypeSRational
	tagTypeFloat
	tagTypeDouble
)

func readTiff(filename string) (*types.MetaImage, error) {
	// Open file
	f, err := os.Open(filename)
	defer f.Close()

	if err != nil {
		return nil, err
	}

	// Read all the file at once
	byts, err := ioutil.ReadAll(bufio.NewReader(f))
	if err != nil {
		return nil, err
	}

	var order binary.ByteOrder
	br := bytes.NewReader(byts)

	// Read header
	var head [4]byte
	binary.Read(br, binary.LittleEndian, &head)

	switch head[0] {
	case 0x49:
		order = binary.LittleEndian
	case 0x4D:
		order = binary.BigEndian
	}

	// Read first IFD offset
	var ifd uint32
	binary.Read(br, order, &ifd)

	br.Seek(int64(ifd), 0)

	// Output image
	metaImage := types.NewMetaImage()

	// Read all dirs
	for true {
		var dirs uint16
		binary.Read(br, order, &dirs)

		var tag, tagType uint16
		var count uint32
		var value [4]byte
		for d := 0; uint16(d) < dirs; d++ {
			binary.Read(br, order, &tag)
			binary.Read(br, order, &tagType)
			binary.Read(br, order, &count)
			binary.Read(br, order, &value)

			// Count number of bytes in value
			byteCount := 0
			switch tagType {
			case tagTypeASCII, tagTypeByte, tagTypeSByte, tagTypeUndefined:
				byteCount++
			case tagTypeShort, tagTypeSShort:
				byteCount += 2
			case tagTypeLong, tagTypeSLong, tagTypeFloat:
				byteCount += 4
			case tagTypeRational, tagTypeSRational, tagTypeDouble:
				byteCount += 8
			}

			// Read from value of at offset position
			if uint32(byteCount)*count <= 4 {
				readValue(metaImage, tag, tagType, count, bytes.NewReader(value[:]), order)
			} else {
				var offset uint32
				binary.Read(bytes.NewReader(value[:]), order, &offset)
				readValue(metaImage, tag, tagType, count, bytes.NewReader(byts[offset:]), order)
			}
		}

		// Read next ifd
		var next uint32
		binary.Read(br, order, &next)
		// Terminate if next ifd is 0
		if next == 0 {
			break
		}

		br.Seek(int64(next), 0)
	}

	// Read image data
	f.Seek(0, 0)
	img, err := tiff.Decode(bufio.NewReader(f))
	metaImage.Image = img

	return metaImage, err
}

// Reads tag value
func readValue(image *types.MetaImage, tag, tagType uint16, count uint32, r *bytes.Reader, order binary.ByteOrder) {
	// Parse value
	switch tagType {
	case tagTypeASCII:
		v := make([]uint8, count-1)
		for i := 0; uint32(i) < count-1; i++ {
			binary.Read(r, order, &v[i])
		}
		image.MetaData[tag] = types.MetaValue{Value: string(v), TagType: tagType, Count: count}
	case tagTypeByte, tagTypeUndefined:
		if count == 1 {
			var v uint8
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]uint8, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeSByte:
		if count == 1 {
			var v int8
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]int8, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeShort:
		if count == 1 {
			var v uint16
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]uint16, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeSShort:
		if count == 1 {
			var v int16
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]int16, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeLong:
		if count == 1 {
			var v uint32
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]uint32, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeSLong:
		if count == 1 {
			var v int32
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]int32, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeFloat:
		if count == 1 {
			var v float32
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]float32, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeDouble:
		if count == 1 {
			var v float64
			binary.Read(r, order, &v)
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: 1}
		} else {
			v := make([]float64, count)
			for i := 0; uint32(i) < count; i++ {
				binary.Read(r, order, &v[i])
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeRational:
		if count == 1 {
			var rat types.Rational
			binary.Read(r, order, &rat)
			image.MetaData[tag] = types.MetaValue{Value: rat, TagType: tagType, Count: 1}
		} else {
			v := make([]types.Rational, count)
			for i := 0; uint32(i) < count; i++ {
				var rat types.Rational
				binary.Read(r, order, &rat)
				v[i] = rat
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	case tagTypeSRational:
		if count == 1 {
			var rat types.SRational
			binary.Read(r, order, &rat)
			image.MetaData[tag] = types.MetaValue{Value: r, TagType: tagType, Count: 1}
		} else {
			v := make([]types.SRational, count)
			for i := 0; uint32(i) < count; i++ {
				var rat types.SRational
				binary.Read(r, order, &rat)
				v[i] = rat
			}
			image.MetaData[tag] = types.MetaValue{Value: v, TagType: tagType, Count: count}
		}
	}
}

// writeTiff writes a MetaImage to a tiff file
func writeTiff(filename string, image *types.MetaImage) error {
	// Create file
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		return err
	}

	f.Write([]byte{0x4D, 0x4D})
	binary.Write(f, binary.BigEndian, uint16(42))
	binary.Write(f, binary.BigEndian, uint32(8))

	var keys []int
	for k := range image.MetaData {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	// Write number of dirs
	binary.Write(f, binary.BigEndian, uint16(len(keys)))

	// This is for storing all tag values that are bigger than 4 bytes,
	// and write them later on
	vOffset := uint32(8 + 2 + 12*len(image.MetaData) + 4)
	vValues := make([]interface{}, 0, len(image.MetaData))

	// Write all dirs
	for _, k := range keys {
		t := image.MetaData[uint16(k)]
		// Write tag
		binary.Write(f, binary.BigEndian, uint16(k))
		binary.Write(f, binary.BigEndian, uint16(t.TagType))
		binary.Write(f, binary.BigEndian, uint32(t.Count))

		// Count number of bytes in value
		byteCount := 0
		switch t.TagType {
		case tagTypeASCII, tagTypeByte, tagTypeSByte, tagTypeUndefined:
			byteCount++
		case tagTypeShort, tagTypeSShort:
			byteCount += 2
		case tagTypeLong, tagTypeSLong, tagTypeFloat:
			byteCount += 4
		case tagTypeRational, tagTypeSRational, tagTypeDouble:
			byteCount += 8
		}

		if uint32(byteCount)*t.Count <= 4 {
			// Write value directly
			buf := bytes.NewBuffer(make([]byte, 0, 4))
			binary.Write(buf, binary.BigEndian, t.Value)
			b := buf.Bytes()
			binary.Write(f, binary.BigEndian, b)

			// Pad with 0s
			for i := 0; i < 4-len(b); i++ {
				binary.Write(f, binary.BigEndian, byte(0))
			}
		} else {
			// Write offset
			binary.Write(f, binary.BigEndian, vOffset)
			vOffset += uint32(byteCount) * t.Count

			// Handle special cases
			vValues = append(vValues, t.Value)
		}
	}

	// Write next ifs offset
	binary.Write(f, binary.BigEndian, uint32(0))

	// Write big tag values
	for _, v := range vValues {
		switch v.(type) {
		case string:
			binary.Write(f, binary.BigEndian, []byte(v.(string)))
			binary.Write(f, binary.BigEndian, byte(0))
		default:
			binary.Write(f, binary.BigEndian, v)
		}
	}

	// Write image

	return nil
}
