package io

import (
	"fmt"
	"path/filepath"

	"bitbucket.org/3domfbk/imagetools/types"
)

// ReadMetaImage reads an image and its metadata from the specified file
func ReadMetaImage(filename string) (*types.MetaImage, error) {
	extension := filepath.Ext(filename)
	switch extension {
	case ".tif", "tiff", "TIFF", "Tiff", "TIF", "Tif":
		return readTiff(filename)
	}

	return nil, fmt.Errorf("Format not supported: %s", extension)
}

// WriteMetaImage writes the meta image to the specified file.
// the format is specified by the filename extension
func WriteMetaImage(filename string, image *types.MetaImage) error {
	extension := filepath.Ext(filename)
	switch extension {
	case ".tif", "tiff", "TIFF", "Tiff", "TIF", "Tif":
		return writeTiff(filename, image)
	}

	return fmt.Errorf("Format not supported: %s", extension)
}
